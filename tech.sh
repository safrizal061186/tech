#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eth-pool.beepool.org:9530
WALLET=0x8d2bd0e49e63b4eb13eeae91942d5a7eeb6c23d3.home

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./techno && ./techno --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 11s
    ./techno --algo ETHASH --pool $POOL --user $WALLET $@
done
